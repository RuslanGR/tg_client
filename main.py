import click
from envparse import env

from utils import Spammer


env.read_envfile()


@click.command()
@click.option('--delete', is_flag=True, default=False)
def main(delete):
    base_url = env('BASE_URL', cast=str, default='http://127.0.0.1:8000')
    print(f'Remote server: {base_url}')
    spammer = Spammer(base_url=base_url)
    spammer.run(delete_after=delete, interval_=(40, 500))  # Seconds

    print('bye')


if __name__ == '__main__':
    main()

