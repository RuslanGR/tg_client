import requests
from time import sleep
from random import uniform
from requests.exceptions import ConnectionError
from progressbar import progressbar

import pyautogui as p
import keyboard


p.FAILSAFE = False


class Spammer(object):
    """Spammer class"""

    __slots__ = ('PROFILE', 'STATUS', 'TEXT')

    def __init__(self, base_url=None):
        """Init"""
        self.PROFILE = f'{base_url}/profiles/'
        self.STATUS = f'{base_url}/status/'
        self.TEXT = f'{base_url}/text/'

    class BanException(Exception):
        """Ban message appear"""

    def mark_as_spammed(self, user_id_, spammed=False, payload: dict=None):
        """Mark user as spammed

        :param user_id_: ID of user
        :param spammed: If user spammed
        :param payload: Dict of additional information
        """
        if payload:
            if payload.get('mark_as_new'):
                requests.post(
                    self.PROFILE, {'user_id': user_id_, 'spammed': spammed, 'new': payload.get('mark_as_new')}
                )
                return
            if payload.get('no_image'):
                requests.post(
                    self.PROFILE, {'user_id': user_id_, 'spammed': spammed, 'no_image': payload.get('no_image')}
                )
                return

        print(f'Spammed: {user_id_}' if spammed else f'Not spammed: {user_id_}')
        requests.post(self.PROFILE, {'user_id': user_id_, 'spammed': spammed})

    def _click(self, x, y, button=None):
        """Click with move function"""
        p.moveTo(x, y, uniform(.9, 1.1), p.easeInQuad)
        self.random_sleep(interval=(.1, .5))
        p.click(button=button)

    @staticmethod
    def _stay_active(time):

        time_passed = 0
        while True:
            p.moveTo(200, 200, 1, p.easeInQuad)
            sleep(.5)
            p.moveTo(800, 200, 1, p.easeInQuad)
            sleep(.5)
            time_passed += 3
            if time_passed >= time:
                return

    def _image_wait_click(self, img, confidence=0.6, time=10, button='left'):
        coords = p.locateOnScreen(img, confidence=confidence)

        time_passed = 0
        while not coords:
            sleep(1)
            coords = p.locateOnScreen(img, confidence=confidence)
            time_passed += 1
            if time_passed == time:
                raise TypeError
        self._image_click(img, confidence=confidence, button=button)

    def _image_click(self, img, confidence: float=1, button='left'):
        x, y = p.locateCenterOnScreen(img, confidence=confidence)
        self._click(x, y, button=button)

    def spam_once(self, user_id, text, delete_after=False):
        """

        Spam to one profile

        :param user_id: id of user
        :param text: text which will send to user with user_id
        :param delete_after: if you need message to be deleted after sending
        :return: None
        """
        try:
            self.random_sleep(.5)
            self._image_wait_click('assets/menu.png', confidence=.7)
            self.random_sleep(.4)
            self._image_wait_click('assets/contacts_logo.png', confidence=.7)
            self.random_sleep(2)
            p.typewrite(f'@{user_id}', interval=uniform(.1, .3))
            self.random_sleep(2)
            p.press('enter')
            self._image_wait_click('assets/write_message.png', confidence=.6)
            self.random_sleep(1)
            keyboard.write(text=text, delay=.23)
            self.random_sleep(2.3)
            p.press('enter')

            # In case we need to delete message after sending
            if delete_after:
                self.random_sleep(interval=(2, 3))
                self._image_wait_click('assets/msg_marker.png', confidence=.9, button='right')
                self.random_sleep()
                self._image_wait_click('assets/msg_delete.png', confidence=.9)
                self.random_sleep()
                self._image_wait_click('assets/empty_box.png', confidence=.8)
                self.random_sleep()
                self._image_wait_click('assets/delete_btn.png', confidence=.7)

        except KeyboardInterrupt:
            self.mark_as_spammed(user_id, spammed=False, payload={'mark_as_new': 1})
        except TypeError as err:
            print('Image not found')
            print(err.args)
            self.mark_as_spammed(user_id, spammed=False, payload={'no_image': 1})
            sleep(3)
        else:
            self.mark_as_spammed(user_id, spammed=True)

    @staticmethod
    def random_sleep(time: float=1, interval: tuple=()):
        """Random sleep function"""
        if interval:
            sleep(uniform(*interval))
            return
        sleep(uniform(time-.2, time+.2))

    def run(self, delete_after, interval_=(3, 6)):
        """Main method which start spamming while server works

        :return: None
        """
        answer = p.alert('Click OK to start')
        if not answer:
            return

        total_count = 0
        process = True
        while process:
            print(self.STATUS)
            try:
                r = requests.get(self.STATUS)
            except ConnectionError:
                print('Not connected')
                self.random_sleep(interval=(3, 6))
                continue

            print('Waiting for connection...')

            status = r.json().get('status')
            if status:  # Status True, program activated
                print('Connected')
                # Get text
                response = requests.get(self.TEXT)
                text = response.json().get('text')

                # Get user id
                response = requests.get(self.PROFILE)
                print(response.json())
                if response.json() == {}:
                    print('No profiles left')
                    self.random_sleep(interval=(3, 6))
                    continue
                user_id = response.json().get('user_id')

                # Spam
                self.spam_once(user_id=user_id, text=text, delete_after=delete_after)
                total_count += 1
                print('Spammed', total_count)

            for _ in progressbar(range(*interval_)):
                sleep(1)
